var $window   = $(window),
	$document = $(document);

(function (window, Modernizr) {
	'use strict';
	var md = new MobileDetect(navigator.userAgent),
		grade = md.mobileGrade();
	Modernizr.addTest({
		mobile: !!md.mobile(),
		phone: !!md.phone(),
		tablet: !!md.tablet(),
		mobilegradea: grade === 'A'
	});
	window.mobileDetect = md;
})(window, Modernizr);

var rfc = {modules : []};
rfc.extend = function(moduleName, moduleData){
	if(!moduleName){
		console.log("Error creating module")
		return;
	}
	if(!moduleData){
		var moduleData = {elements: {}, init: function(){console.log("Empty init for module")}};
	}
	this[moduleName] = moduleData;
	this.modules.push( moduleData );
	return moduleData;
}
rfc.init = function(){
	var totalModules = rfc.modules.length;
	for(var k = 0; k < totalModules; k++){
		rfc.modules[k].init();
	}
}

//*******************************************
//
//	module
//
//*******************************************

rfc.extend("common", {

	init: function() {

		var self = this;
		
		this.elements = {};
		this.elements.$html = $("html");
		this.elements.$body = $("body");
		this.elements.$up   = $(".go-up");
		this.elements.$breadcrumbs = $(".breadcrumbs-block__wrap")
		
		this.elements.$clients = $(".clients-block__pic").filter(":hidden");
		
		$(window, document).on('load',function() {
			setTimeout(function(){
				self.elements.$body.removeClass('loading');
			}, 0);
		});
		
		if ($window.width() < 641) {
			$window.on("load resize", function(e) {
				var ulWidth = 0;
				self.elements.$breadcrumbs.find("li").each(function(i) {
					ulWidth += $(this).width();
				});
				if (ulWidth > self.elements.$breadcrumbs.width()) {
					console.log(ulWidth);
					self.elements.$breadcrumbs.scrollLeft(ulWidth);
				}
			});
		}
		
		if ($("html").attr("data-tpl")==="index") {
			
			if ($window.width() > 640) {
				$('.header-nav__list').onePageNav({
					currentClass: '__active',
					changeHash: false,
					scrollSpeed: 900,
					scrollOffset: 80,
					scrollThreshold: 0.3,
					filter: ':not(.no-scroll)',
					begin: function () {
						//Hack so you can click other menu items after the initial click
						$('body').append('<div id="device-dummy" style="height: 1px;"></div>');
					},
					end: function () {
						$('#device-dummy').remove();
					}
				});
			}
			
			// init controller
			//var controller = new ScrollMagic.Controller();
			var controller = new ScrollMagic.Controller({
				globalSceneOptions: {
					duration: '70%',
					triggerHook: .025,
					reverse: true
				}
			});
			
			/*controller.scrollTo(function(target) {
				console.log(target)
				TweenMax.to(window, 0.5, {
					scrollTo : {
						y : target-85, // scroll position of the target along y axis
						autoKill : true // allows user to kill scroll action smoothly
					},
					ease : Cubic.easeInOut
				});
				
			});
			
			var scenes = {
				'intro': {
					'fireproof': 'anchor1'
				},
				'scene2': {
					'auxiliary': 'anchor2'
				},
				'scene3': {
					'about': 'anchor3'
				},
				'scene4': {
					'partners': 'anchor4'
				},
				'scene5': {
					'partners': 'anchor5'
				},
				'scene6': {
					'contacts': 'anchor6'
				}
			}
			
			for(var key in scenes) {
				// skip loop if the property is from prototype
				if (!scenes.hasOwnProperty(key)) continue;
				
				var obj = scenes[key];
				
				for (var prop in obj) {
					// skip loop if the property is from prototype
					if(!obj.hasOwnProperty(prop)) continue;
					
					new ScrollMagic.Scene({ triggerElement: '#' + prop })
						.setClassToggle('#' + obj[prop], 'active')
						.addTo(controller);
				}
			}
			
			//  Bind scroll to anchor links
			var anchor_nav = document.querySelector('.header-nav');
			
			anchor_nav.addEventListener('click', function(e) {
				var target = e.target,
					id     = target.getAttribute('href');
				
				if(id !== null) {
					if(id.length > 0) {
						e.preventDefault();
						controller.scrollTo(id);
						
						if(window.history && window.history.pushState) {
							history.pushState("", document.title, id);
						}
					}
				}
			});*/
			
			// build scene
			var scene1 = new ScrollMagic.Scene({triggerElement: ".info-block", duration: 200})
			 .addTo(controller)
			 //.addIndicators() // add indicators (requires plugin)
			 .on("start", function (e) {
				 var complete = function () {
					 TweenMax.staggerTo($(".info-block__fact"), 1, {opacity: 1,y: 0}, 0.15 );
				 };
			    TweenMax.to($(".info-block__title"), 1, {x: 0, onComplete: complete} );
			 });
		}
		
		$document.on("ready", function(e) {
			
			if ($(".product-gallery__block").is("div")) {
				$('a[rel="gallery"]').featherlightGallery();
			}
			
		});

		if (Modernizr.mobile) {
			$(".phone-number").each(function() {
				$(this).replaceWith(
					$("<a href='tel:" + this.innerHTML + "'>" + this.innerHTML + "</a>")
				);
			});
		}
		
		this.elements.$up.on("click", function(e) {
			$("html, body").animate({ scrollTop: 0 }, 600);
		});
		$(".clients-block__btn-more").on("click", function(e) {
			$(this).toggle();
			self.elements.$clients.toggle();
		});
		
		$(".show-left__btn").on("click", function(e) {
			TweenMax.to($(".catalog-side"), 0.5, {x: 0, alpha:1} );
			this.disableScrollFn = function(e) {
				e.preventDefault(); e.stopPropagation()
			};
			self.elements.$body.on('scroll', this.disableScrollFn);
			self.elements.$html.css("overflow","hidden");
			self.elements.$body.css("overflow","hidden");
		});
		
		$(".catalog-side .close-btn").on("click", function(e) {
			TweenMax.to($(".catalog-side"), 0.5, {x: -600, alpha:1} );
			self.elements.$body.off('mousewheel touchmove', this.disableScrollFn);
			self.elements.$html.css("overflow","");
			self.elements.$body.css("overflow","");
		});
	}

});

rfc.extend("menu", {

	init: function() {

		var self = this;
		
		this.elements = {};
		this.elements.$html = $("html");
		this.elements.$body           = $("body");
		this.elements.$logo      = $(".header-logo");
		this.elements.$headerSandwich = $(".header-sandwich");
		this.elements.$menuBlock      = $(".header-nav");
		this.elements.$menuLink      = $(".header-nav__link");
		
		$window.on("scroll", function(e) {
			if ($window.scrollTop() >/*$window.height()*/87) {
				$(".header-wrap").addClass("__scrolled");
			} else {
				$(".header-wrap").removeClass("__scrolled");
			}
		});

		this.elements.$headerSandwich.unbind("click").on("click", function(e) {
			if ($(this).hasClass("__active")) {
				self.hideMenu();
			} else {
				self.showMenu();
			}
		});
		
		this.elements.$menuLink.on("click", function(e) {
			if ($("html").attr("data-tpl")=="index") {
				$a = $($(this).attr('href'));
				if ($window.width() < 641) {
					$('html,body').scrollTop($a.offset().top);
					self.hideMenu();
				} else {
					$('html,body').animate({scrollTop: $a.offset().top}, 600);
				}
				e.preventDefault();
				return false;
			}
		});

		$(".phone-number").each(function() {
			$(this).replaceWith(
				$("<a href='tel:" + this.innerHTML + "'>" + this.innerHTML + "</a>")
			);
		});

	},

	showMenu: function() {
		var self = this;
		this.elements.$menuBlock.show();
		this.elements.$headerSandwich.addClass("__active");
		//TweenMax.to(self.elements.$menuBlock, 1, {x: 0, alpha:1} );
		this.disableScrollFn = function(e) {
			e.preventDefault(); e.stopPropagation()
		};
		//self.elements.$body.on('scroll', this.disableScrollFn);
		self.elements.$html.css("overflow","hidden");
		self.elements.$body.css("overflow","hidden");
	},

	hideMenu: function() {
		var self = this;
		//TweenMax.to(self.elements.$menuBlock, 1, {x: -600, alpha:1} );
		this.elements.$menuBlock.hide();
		self.elements.$headerSandwich.removeClass("__active");
		//self.elements.$body.off('mousewheel touchmove', this.disableScrollFn);
		self.elements.$html.css("overflow","");
		self.elements.$body.css("overflow","");
	}

});

rfc.extend("mapIndex", {

	init: function() {
		this.block = false;
		var self = this;
		
		this.elements = {};
		this.elements.$map  = $(".partners-block__map");
		this.elements.$country  = this.elements.$map.find(".partners-block__country");
		this.elements.$trigger = this.elements.$map.find("[data-flag]");
		this.elements.$link = $(".partners-block__link");
		this.elements.$baloon  = this.elements.$map.find(".partners-block__baloon");
		this.elements.$partner = this.elements.$map.find(".partners-country__item");
		this.elements.$close = this.elements.$baloon.find(".close-btn");
		
		this.elements.$trigger.on("click", function(e) {
			var baloon = self.elements.$baloon.filter("[data-country='"+$(this).attr("data-flag")+"']");
			if (baloon.is(":visible")) {
				baloon.hide();
				self.elements.$partner.show();
			} else {
				self.elements.$baloon.hide();
				self.elements.$partner.show();
				baloon.show();
				self.baloonSet(baloon);
			}
		});
		
		this.elements.$link.on("click", function(e) {
			self.elements.$partner.show();
			//self.baloonShow($(this).attr("data-partner"));
			var partner = self.elements.$partner.filter("[data-partner='"+$(this).attr("data-partner")+"']");
			console.log($(this).attr("data-partner"));
			var baloon = partner.closest(self.elements.$baloon);
			console.log(baloon);
			if ($(this).parent("li").hasClass("__active")) return;
			$(".partners-block__item").removeClass("__active");
			$(this).parent("li").addClass("__active");
			self.elements.$baloon.hide();
			baloon.show();
			self.elements.$partner.not(partner).hide();
			self.baloonSet(baloon);
			return false;
		});
		
		this.elements.$close.on("click", function(e) {
			self.elements.$baloon.hide();
			self.elements.$partner.show();
		});
		
		$document.click(function(e) {
			if (!$(e.target).closest(self.elements.$baloon).length && !$(e.target).closest(self.elements.$trigger).length && !$(e.target).closest(self.elements.$link).length && !$(e.target).closest((".partners-block__item")).length){
				self.elements.$baloon.hide();
				self.elements.$partner.show();
			}
		});

	},
	
	animateMap: function() {
		
		var self = this;
		
	},

	baloonSet: function(el) {

		var self = this;
		el.find("li:visible").length > 2 ? el.addClass("more2child") : el.removeClass("more2child");
		console.log(el.find("li:visible").length);

	}
	
	/*baloonHide: function() {

		var self = this;
		this.elements.$short.toggle();
		this.elements.$full.toggle();
		this.elements.$btn.html("Подробнее");

	}*/

});

rfc.extend("productGallery", {
	
	init: function() {
		
		if (!$(".product-gallery__block").is("div")) return;
		var self = this;
		
		this.elements = {};
		this.elements.$cont = $(".product-gallery__block");
		this.elements.$pics = $(".product-pics__list");
		this.elements.$picsItem = this.elements.$cont.find(".product-pics__item");
		this.elements.$picsItemCur = this.elements.$picsItem.filter(":first-child");
		this.elements.$thumbs = $(".product-thumbs__list");
		this.elements.$thumbsItem = this.elements.$cont.find(".product-thumbs__item");
		
		this.elements.$length = this.elements.$picsItem.length;
		
		this.elements.$thumbsItem.on("click", function(e) {
			self.elements.$thumbsItem.removeClass("__active");
			$(this).addClass("__active");
			self.moveTo($(this).index());
		});
	},
	
	set: function() {
		
		var self = this;
		self.moveTo(0);
		
	},
	
	moveLeft: function() {
		
		var self = this;
		
		//self.block = true;
		this.elements.$arrLeft.removeClass("__disabled");
		
		var width = this.elements.$pics.outerWidth();
		var count  = this.elements.$pics.attr("data-count");
		count++;
		this.elements.$pics.attr("data-count",count);
		
		/*TweenMax.to(this.elements.$picsItem[0], 0.3, { marginLeft: -width * count, onComplete: function() {
		 self.block = false;
		 } });*/
		
		this.elements.$picsItemCur.removeClass("__active");
		this.elements.$picsItemCur = this.elements.$picsItemCur.next();
		this.elements.$picsItemCur.addClass("__active");
		//this.elements.$dotNav.find("li").removeClass("__active").filter("[data-nav='" + count + "']").addClass("__active");
		
		if (count == 0) this.elements.$arrLeft.addClass("__disabled");
		if (count == this.elements.$length - 1) this.elements.$arrRight.addClass("__disabled");
		
	},
	
	moveRight: function() {
		
		var self = this;
		
		//self.block = true;
		this.elements.$arrRight.removeClass("__disabled");
		
		var width = this.elements.$pics.outerWidth();
		var count  = this.elements.$pics.attr("data-count");
		count--;
		this.elements.$pics.attr("data-count",count);
		
		/*TweenMax.to(this.elements.$picsItem[0], 0.3, {
		 marginLeft: -width * count, onComplete: function () {
		 self.block = false;
		 }
		 });*/
		
		this.elements.$picsItemCur.removeClass("__active");
		this.elements.$picsItemCur = this.elements.$picsItemCur.prev();
		this.elements.$picsItemCur.addClass("__active");
		//this.elements.$dotNav.find("li").removeClass("__active").filter("[data-nav='" + count + "']").addClass("__active");
		
		if (count == 0) this.elements.$arrLeft.addClass("__disabled");
		if (count == this.elements.$length) this.elements.$arrRight.addClass("__disabled");
	},
	
	moveTo: function(index) {
		
		var self = this;
		
		var count  = index;
		this.elements.$pics.attr("data-count",count);
		
		this.elements.$picsItemCur.removeClass("__active");
		this.elements.$picsItemCur = this.elements.$picsItem.filter(":eq("+ index +")");
		this.elements.$picsItemCur.addClass("__active");
		
	}
	
});

$(rfc.init);